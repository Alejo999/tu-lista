package trabajo.poo.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import trabajo.poo.app.db.Conexion;
import trabajo.poo.app.db.Inventarios;

public class ActivityInventario extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, TextView.OnEditorActionListener {
    private Button AñadirAInventario;
    private ListView mListView;
    private EditText mEditText;
    private List<String> mLista = new ArrayList<>();
    private ArrayAdapter<String> mAdapter;
    private EditText buscar;
    TextView textid, textnombre;
    Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);

        textid = (TextView) findViewById(R.id.textViewId);
        textnombre= (TextView) findViewById(R.id.textViewNombre);

        Bundle extras = getIntent().getExtras();
        int id = extras.getInt(Inventarios.CAMPO_ID);
        String tipoInventario = extras.getString(Inventarios.CAMPO_NOMBREINVENTARIO);
        textid.setText(id+"");
        textnombre.setText(tipoInventario);


        AñadirAInventario= (Button) findViewById(R.id.buttonAñadirAInventario);
        AñadirAInventario.setOnClickListener(this);

        mEditText=(EditText) findViewById(R.id.editTextTextArticuloNuevo);
        buscar=(EditText) findViewById(R.id.editTextTextBuscar);
        buscar.setOnEditorActionListener(this);


        mListView=(ListView) findViewById(R.id.listView);
        mListView.setOnItemClickListener(this);


        SharedPreferences sp = getSharedPreferences("m11",MODE_PRIVATE);
        Set<String> lista = sp.getStringSet("lista", new HashSet<>());
        mLista= new ArrayList<String>(lista);

        mAdapter = new ArrayAdapter <> (this,android.R.layout.simple_list_item_1,mLista);
        mListView.setAdapter(mAdapter);

        buscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
             case R.id.buttonAñadirAInventario:
                String texto = mEditText.getText().toString().trim();
                Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
                mLista.add(texto);
                mEditText.getText().clear();
                mAdapter = new ArrayAdapter <> (this,android.R.layout.simple_list_item_1,mLista);
                mListView.setAdapter(mAdapter);

                Set<String>set = new HashSet<String>(mLista);
                SharedPreferences.Editor editor= getSharedPreferences("m11",MODE_PRIVATE).edit();
                editor. putStringSet("lista", set);
                editor.commit();

                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getId()== mListView.getId()){
            String seleccionado = adapterView.getItemAtPosition(i).toString();
            Toast.makeText(getApplicationContext(), seleccionado, Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor edit = getSharedPreferences("m12",MODE_PRIVATE).edit();
            edit.putString("elemento", seleccionado);
            edit.commit();
            Intent intent=new Intent(this,ActivityLista.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (textView.getId()== buscar.getId()){
            String nombre = buscar.getText().toString();
            Toast.makeText(getApplicationContext(), nombre, Toast.LENGTH_SHORT).show();

        }
        return false;
    }
}