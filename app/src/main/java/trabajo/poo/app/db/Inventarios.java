package trabajo.poo.app.db;

public class Inventarios {
    private int id;
    private String NombreInventario;

    public Inventarios(int id, String inventario) {
        this.id = id;
        this.NombreInventario = inventario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInventario() {
        return NombreInventario;
    }

    public void setInventario(String inventario) {
        this.NombreInventario = inventario;
    }

    public static final String CREAR_TABLA_INVENTARIO = "CREATE TABLE inventario (id INTEGER, NombreInventario TEXT)";
    public static final String DROPEAR_TABLA_INVENTARIO = "DROP TABLE IF EXISTS inventario ";

    public static final String TABLA_INVENTARIO = "inventario";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_NOMBREINVENTARIO= "NombreInventario";

}
