package trabajo.poo.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexion extends SQLiteOpenHelper {
    public Conexion(@Nullable Context context) {
        super(context, BD_Inventario, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       sqLiteDatabase.execSQL(Inventarios.CREAR_TABLA_INVENTARIO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(Inventarios.DROPEAR_TABLA_INVENTARIO);
        onCreate(sqLiteDatabase);


    }
    public static final String BD_Inventario="bd_inventario";
}
