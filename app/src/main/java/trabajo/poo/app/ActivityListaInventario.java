package trabajo.poo.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

import trabajo.poo.app.db.Conexion;
import trabajo.poo.app.db.Inventarios;

public class ActivityListaInventario extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    Button button;
    ListView lisi;
    private Conexion conexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_inventario);
        button = (Button) findViewById(R.id.buttonCrear);
        button.setOnClickListener(this);

        this.conexion = new Conexion(this);
        lisi = (ListView) findViewById(R.id.listainventario);

        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,getLista());
        lisi.setAdapter(adaptador);
        lisi.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==button.getId()){
            Intent i = new Intent(this, ActivityTipo.class);
            startActivity(i);
        }
    }


    private List<Inventarios> listainventarios;
    private List<String> getLista() {
        listainventarios = new LinkedList<Inventarios>();
        List<String> listatipoinventario = new LinkedList<String>();

        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] campos = {Inventarios.CAMPO_ID,Inventarios.CAMPO_NOMBREINVENTARIO};

        Cursor cursor = db.query(Inventarios.TABLA_INVENTARIO, campos, null, null, null, null, null );
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String tipoInventario = cursor.getString(1);


            Inventarios i = new Inventarios(id, tipoInventario);
            listainventarios.add(i);
            listatipoinventario.add(i.getInventario());
        }
        return listatipoinventario;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        Intent intent = new Intent(this, ActivityInventario.class);

        Inventarios i = listainventarios.get(position);
        Bundle extras = new Bundle();
        extras.putInt(Inventarios.CAMPO_ID,i.getId());
        extras.putString(Inventarios.CAMPO_NOMBREINVENTARIO,i.getInventario());
        intent.putExtras(extras);

        startActivity(intent);

    }
}