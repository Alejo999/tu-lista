package trabajo.poo.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ActivityLista extends AppCompatActivity implements View.OnClickListener {
    private EditText Texto;
    private Button Añadir;
    private Button Terminar;
    private ListView ultima;
    private List<String> uLista = new ArrayList<>();
    private ArrayAdapter<String> Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Texto = (EditText) findViewById(R.id.textPortada);
        SharedPreferences sp = getSharedPreferences("m12",MODE_PRIVATE);
        String seleccionado = sp.getString("elemento","");
        Texto.setText(seleccionado);

        Añadir= (Button) findViewById(R.id.buttonAñadir);
        Añadir.setOnClickListener(this);
        Terminar= (Button) findViewById(R.id.buttonTerminarCompras);
        Terminar.setOnClickListener(this);
        
        ultima=(ListView) findViewById(R.id.ultimalista);

        SharedPreferences tt = getSharedPreferences("m13",MODE_PRIVATE);
        Set<String> lista = tt.getStringSet("utlista", new HashSet<>());
        uLista= new ArrayList<String>(lista);
        Adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, uLista);
        ultima.setAdapter(Adapter);
        
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonTerminarCompras:
                uLista.clear();


            case R.id.buttonAñadir:
                String texto = Texto.getText().toString().trim();
                Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
                uLista.add(texto);
                Texto.getText().clear();
                Adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, uLista);
                ultima.setAdapter(Adapter);

                Set<String>set = new HashSet<String>(uLista);
                SharedPreferences.Editor editor= getSharedPreferences("m13",MODE_PRIVATE).edit();
                editor. putStringSet("utlista", set);
                editor.commit();

                break;
        }
    }
}