package trabajo.poo.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import trabajo.poo.app.db.Conexion;
import trabajo.poo.app.db.Inventarios;

public class ActivityTipo extends AppCompatActivity implements View.OnClickListener {
    Button nuevo;
    EditText id;
    EditText tipoInventario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo);

        nuevo = (Button)findViewById(R.id.buttonNuevo);
        nuevo.setOnClickListener(this);

        id = (EditText) findViewById(R.id.editTextId);
        tipoInventario = (EditText) findViewById(R.id.editTextTipo);


    }

    @Override
    public void onClick(View view) {
        if (view.getId()==nuevo.getId()){
            String ids = id.getText().toString();
            String invent = tipoInventario.getText().toString();

            if(!ids.equals("")&&!invent.equals("")) {

                Conexion conexion = new Conexion(this);
                SQLiteDatabase db = conexion.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(Inventarios.CAMPO_ID, ids);
                values.put(Inventarios.CAMPO_NOMBREINVENTARIO, invent);

                db.insert(Inventarios.TABLA_INVENTARIO, Inventarios.CAMPO_ID, values);
                db.close();

                Toast.makeText(this, "Nuevo Inventario Agregado", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, ActivityListaInventario.class);
                startActivity(i);
            } else {
                Toast.makeText(this, "No se pudo realizar la acción", Toast.LENGTH_SHORT).show();
            }

        }


    }

}