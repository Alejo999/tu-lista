package trabajo.poo.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button CrearListaDeCompras;
    private Button Almacen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        CrearListaDeCompras = (Button) findViewById(R.id.buttonCrearListaDeCompras);
        CrearListaDeCompras.setOnClickListener(this);

        Almacen = (Button) findViewById(R.id.buttonAlmacen);
        Almacen.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.buttonCrearListaDeCompras) {
            Intent intent = new Intent(this, ActivityLista.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Lista de compras", Toast.LENGTH_SHORT).show();
        }
        if (view.getId() == R.id.buttonAlmacen) {
            Intent intent = new Intent(this, ActivityListaInventario.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(), "Almacen", Toast.LENGTH_SHORT).show();
        }
    }
}